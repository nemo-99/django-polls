import datetime
from django.utils import timezone
from .models import Question
from django.test import TestCase
from django.urls import reverse

# Create your tests here.


class QuestionModelTestCase(TestCase):

    def testCheckFutureDatesAreNotRecent(self):
        q = Question(pubDate=timezone.now() + datetime.timedelta(days=30))
        self.assertIs(q.wasPublishedRecently(), False)

    def testCheckOlderDatesAreNotRecent(self):
        q = Question(pubDate=timezone.now() - datetime.timedelta(days=30))
        self.assertIs(q.wasPublishedRecently(), False)

    def testCheckRecentDatesAreRecent(self):
        q = Question(pubDate=timezone.now() - datetime.timedelta(hours=23))
        self.assertIs(q.wasPublishedRecently(), True)


class IndexViewTests(TestCase):

    def testFutureQuestionsAbsent(self):
        q = Question("What's up", timezone.now() + datetime.timedelta(days=2))
        response = self.client.get(reverse("polls:index"))
        self.assertContains(response, "No questions available for now")
        self.assertQuerysetEqual(response.context["questionsList"], [])
