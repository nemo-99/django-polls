import datetime
from django.contrib import admin

from django.db import models
from django.utils import timezone

# Create your models here.


class Question(models.Model):
    questionText = models.CharField(max_length=200)
    pubDate = models.DateTimeField('date published')

    def __str__(self) -> str:
        return self.questionText

    @admin.display(
        boolean=True,
        ordering="pubDate",
        description="Published recently ?"
    )
    def wasPublishedRecently(self) -> bool:
        return timezone.now() - datetime.timedelta(days=1) <= self.pubDate <= timezone.now()


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choiceText = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self) -> str:
        return self.choiceText
