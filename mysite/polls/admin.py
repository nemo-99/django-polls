from django.contrib import admin
from .models import Question, Choice

# Register your models here.


class ChoiceAdmin(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('questionText', 'pubDate', 'wasPublishedRecently')
    list_filter = ['pubDate']
    search_fields = ['questionText']
    fieldsets = [
        (None, {'fields': ['questionText']}),
        ('Date Information', {'fields': ['pubDate']}),
    ]
    inlines = [ChoiceAdmin]


admin.site.register(Question, QuestionAdmin)
