from django.utils import timezone
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.urls import reverse
from django.views import generic

from .models import Question
# Create your views here.


class IndexView(generic.ListView):
    model = Question
    template_name = "polls/index.html"
    context_object_name = "questionsList"

    def get_queryset(self):
        return Question.objects.filter(pubDate__lte=timezone.now()).order_by("-pubDate")[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = "polls/detail.html"


class ResultView(generic.DetailView):
    model = Question
    template_name = "polls/result.html"


def vote(request, questionId):
    question = get_object_or_404(Question, pk=questionId)
    try:
        choice = question.choice_set.get(pk=request.POST["choice"])
    except KeyError:
        return render(request, "polls/detail.html", context={"question": question, "errorMessage": "Please select an option"})
    else:
        choice.votes += 1
        choice.save()
        return HttpResponseRedirect(reverse("polls:result", args=(question.id,)))
